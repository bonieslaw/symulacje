﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dyfrakcja
{
    public partial class Dyfrakcja : Form
    {
        public WaveEngine waveEngine;

        public int drawThickness = 10;
        public bool isDrawingObstaclesOn = true;

        public Dyfrakcja()
        {
            InitializeComponent();

            waveEngine = new WaveEngine(this);
            waveEngine.run();
        }

        private void Dyfrakcja_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                try
                {
                    waveEngine.OscillatorPosition = new Point((int)(((float)e.X / (float)ClientSize.Width) * (float)waveEngine.Size), (int)(((float)e.Y / (float)ClientSize.Height) * (float)waveEngine.Size));
                    waveEngine.IsOscillatorActive = true;
                }
                catch {}
            }
            else if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                try
                {
                    ThreadPool.QueueUserWorkItem((Object arg1) =>
                    {
                        waveEngine.setParticles(new Rectangle((int)(((float)e.X / (float)ClientSize.Width) * (float)waveEngine.Size),
                                               (int)(((float)e.Y / (float)ClientSize.Height) * (float)waveEngine.Size),
                                               drawThickness, drawThickness), Convert.ToSingle(isDrawingObstaclesOn),
                                               WaveEngine.ParticleAttribute.FIXITY);
                    });
                }
                catch
                { }
            }
        }

        private void Dyfrakcja_FormClosing(object sender, FormClosingEventArgs e)
        {
            waveEngine.Dispose();
        }

        private void Dyfrakcja_Load(object sender, EventArgs e)
        {

        }
    }
}
