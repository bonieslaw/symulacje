﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace Dyfrakcja
{
    public class WaveEngine
    {
        Bitmap bitmap;
        BitmapData bitmapData;

        float[] vertexHeight;
        float[] vertexVelocity;
        float[] vertexAcceleration;
        float[] vertexSustainability;
        bool[] isObstacleVertex;
        float particleMass = 0.1f;
        float particleMaximumHeight = 500f;
        float resolutionOfParticlesMovement = 20f; // TODO: zmienić nazwę na bardziej odpowiednią

        float sustainability = 1000f; // anti-damping
        int delayInMilisec = 1;
        float oscillatorPhase = 0f;
        float frequency = 0.2f;
        float powerExertedOnParticles = 1.0f;

        
        
        

        BufferedGraphics graphics; // do minimalizacji migotania ( podwójnie zbufforowana 
                                   // grafika do renderingu . Poczytac o tym wiecej)
        BufferedGraphicsContext graphicsContext;

        Thread forceCalculationWorker;
        Mutex mutex;

        bool isThreadWorking = false;
        bool isDisposed = false;

        bool isOscillatorActive = false;

        int oscillatorIndex = 0;

        Color color1 = Color.Black;
        Color color2 = Color.Magenta;
        Color obstacleColor = Color.BurlyWood;

        int absorbtionOffset = 10;
        float minimalSustainability = 2f;
        bool isEdgeAbsobtion = true;

        int wavePoolSize = 200; // oznacza zarówno szerokość jak i wysokość


        Control control;

        public enum ParticleAttribute
        {
            HEIGHT = 1,
            VELOCITY = 2,
            ACCELERATION = 4,
            SUSTAINABILITY = 8,
            FIXITY = 16,
            ALL = 32,
        }

        public Color ObstacleColor
        {
            get
            {
                return obstacleColor;
            }
            set
            {
                mutex.WaitOne();
                obstacleColor = value;
                mutex.ReleaseMutex();
            }
        }

        public int Size
        {
            get { return wavePoolSize; }

            set
            {
                if( wavePoolSize >= 1f)
                {
                    mutex.WaitOne();
                    wavePoolSize = value;
                    setPool();
                    mutex.ReleaseMutex();
                }
            }
        }

        public bool IsOscillatorActive
        {
            get { return IsOscillatorActive; }

            set
            {
                mutex.WaitOne();
                isOscillatorActive = value;
                setSustainability();
                mutex.ReleaseMutex();
            }
        }

        public Point OscillatorPosition
        {
            get { return new Point(oscillatorIndex % wavePoolSize, (int)Math.Floor((float)oscillatorIndex / (float)wavePoolSize)); }
            set
            {
                if(value.X + value.Y * wavePoolSize < wavePoolSize * wavePoolSize)
                {
                    mutex.WaitOne();
                    oscillatorIndex = value.X + value.Y * wavePoolSize;
                    setSustainability();
                    mutex.ReleaseMutex();
                }
            }
        }

        public WaveEngine(Control control)
        {
            this.control = control;
            control.Resize += new EventHandler(controlResize);
            setPool();
            mutex = new Mutex();
            forceCalculationWorker = new Thread(() =>
           {
               while (!isDisposed)
               {
                   try
                   {
                       while(isThreadWorking)
                       {
                           mutex.WaitOne();
                           int startTime = System.Environment.TickCount;
                           while(System.Environment.TickCount - startTime < delayInMilisec)
                           {
                               calculateForces();
                           }
                           generateBitmap();
                           graphics.Graphics.DrawImage(bitmap, 0, 0, control.ClientSize.Width, control.ClientSize.Height);
                           graphics.Render();
                           mutex.ReleaseMutex();
                           Thread.Sleep(delayInMilisec);
                       }
                   }
                   catch
                   {
                       isThreadWorking = false;
                       mutex.ReleaseMutex();
                   }
                   Thread.Sleep(0);
               }
           });
            forceCalculationWorker.Start();

        }

        public void run()
        {
            isThreadWorking = true;          
        }

        void controlResize(object sender, EventArgs e)
        {
            ThreadPool.QueueUserWorkItem( (Object arg1) =>
            {
                mutex.WaitOne();
                setPool();
                mutex.ReleaseMutex();
            } );
        }

        void setPool()
        {
            clearGraphics();
            graphicsContext = new BufferedGraphicsContext();
            graphics = graphicsContext.Allocate( control.CreateGraphics(),
                                                 control.ClientRectangle  );

            vertexHeight = new float[wavePoolSize * wavePoolSize];
            vertexVelocity = new float[wavePoolSize * wavePoolSize];
            vertexAcceleration = new float[wavePoolSize * wavePoolSize];
            isObstacleVertex = new bool[wavePoolSize * wavePoolSize];
            vertexSustainability = new float[wavePoolSize * wavePoolSize];

            setSustainability();
        }

        void clearGraphics()
        {
            if(graphics != null)
            {
                graphics.Dispose();
            }

            if(graphicsContext != null)
            {
                graphicsContext.Dispose();
            }
        }

        void calculateForces()
        {
            float totalHeight = 0f;

            // do zliczania siły wywieranej na poszczególne cząsteczki
            for(int i = 0; i < vertexHeight.Length; ++i)
            {
                if( isObstacleVertex[i] )
                {
                    vertexHeight[i] = 0;
                    vertexVelocity[i] = 0;
                    vertexAcceleration[i] = 0;
                    continue;
                }

                if(i == oscillatorIndex && isOscillatorActive)
                {
                    vertexVelocity[i] = 0;
                    vertexAcceleration[i] = 0;
                    vertexHeight[i] = particleMaximumHeight * (float)Math.Sin(oscillatorPhase);
                    oscillatorPhase += frequency;

                    if(oscillatorPhase >= 2f * (float)Math.PI)
                    {
                        oscillatorPhase -= (float)Math.PI * 2f;
                    }

                    continue;
                }

                vertexAcceleration[i] = 0;
                totalHeight += vertexHeight[i];
                float neighborParticlesHeightsSum = 0;
                int numberOfParticles = 0;

                // Góra

                // czy nie wychodzi poza zakres...
                if ( !(i >= 0 && i < wavePoolSize) )
                {
                    if( !isObstacleVertex[i - wavePoolSize] )
                    {
                        neighborParticlesHeightsSum += vertexHeight[i - wavePoolSize];
                        numberOfParticles++;
                    }
                }

                // prawy-górny
                if( !( (i+1) % wavePoolSize == 0 || (i >= 0 && i < wavePoolSize) ) )
                {
                    if( !isObstacleVertex[i - wavePoolSize + 1] )
                    {
                        neighborParticlesHeightsSum += vertexHeight[i - wavePoolSize + 1];
                        numberOfParticles++;
                    }
                }

                // prawy
                if( !( (i + 1) % wavePoolSize == 0 ) )
                {
                    if( !isObstacleVertex[i + 1] )
                    {
                        neighborParticlesHeightsSum += vertexHeight[i + 1];
                        numberOfParticles++;
                    }
                }

                // prawy-dolny
                if( !( (i + 1) % wavePoolSize == 0 || (i >= (wavePoolSize * wavePoolSize) - wavePoolSize && i < (wavePoolSize * wavePoolSize) ) ) )
                {
                    if( !isObstacleVertex[i + wavePoolSize + 1] )
                    {
                        neighborParticlesHeightsSum += vertexHeight[i + wavePoolSize + 1];
                        numberOfParticles++;
                    }
                }

                // dół
                if( !( i >= (wavePoolSize * wavePoolSize) - wavePoolSize  && i < (wavePoolSize * wavePoolSize) ) )
                {
                    if( !isObstacleVertex[i + wavePoolSize] )
                    {
                        neighborParticlesHeightsSum += vertexHeight[i + wavePoolSize];
                        numberOfParticles++;
                    }
                }

                // lewy-dolny
                if( !( i % wavePoolSize == 0 || ( i >= (wavePoolSize * wavePoolSize) - wavePoolSize && i < (wavePoolSize * wavePoolSize) ) ) )
                {
                    if( !isObstacleVertex[i + wavePoolSize - 1] )
                    {
                        neighborParticlesHeightsSum += vertexHeight[i + wavePoolSize - 1];
                        numberOfParticles++;
                    }
                }

                // lewy
                if( !(i % wavePoolSize == 0) )
                {
                    if( !isObstacleVertex[i -1] )
                    {
                        neighborParticlesHeightsSum += vertexHeight[i - 1];
                        numberOfParticles++;
                    }
                }

                // lewy-górny
                if( !( i % wavePoolSize == 0 || (i >= 0 && i < wavePoolSize) ) )
                {
                    if( !isObstacleVertex[i - wavePoolSize - 1] )
                    {
                        neighborParticlesHeightsSum += vertexHeight[i - wavePoolSize - 1];
                        numberOfParticles++;
                    }
                }

                if(numberOfParticles != 0)
                {
                    neighborParticlesHeightsSum = neighborParticlesHeightsSum / (float)numberOfParticles;

                    if(powerExertedOnParticles != 1.0f)
                    {
                        vertexAcceleration[i] += Math.Sign(neighborParticlesHeightsSum - vertexHeight[i]) * (float)Math.Pow(Math.Abs(vertexHeight[i] - neighborParticlesHeightsSum), powerExertedOnParticles) / particleMass;
                    }
                    else
                    {
                        vertexAcceleration[i] += -(vertexHeight[i] - neighborParticlesHeightsSum) / particleMass;
                    }
                }

                // tłumienie
                vertexAcceleration[i] -= vertexVelocity[i] / vertexSustainability[i];

                // sprawdzenie limitów
                if ( vertexAcceleration[i] > particleMaximumHeight )
                {
                    vertexAcceleration[i] = particleMaximumHeight;
                }
                else if(vertexAcceleration[i] < -particleMaximumHeight )
                {
                    vertexAcceleration[i] = -particleMaximumHeight;
                }
            }

            //shifting...
            float shifting = -totalHeight / (float)vertexHeight.Length;

            for (int i = 0; i < vertexHeight.Length; i++)
            {
                vertexVelocity[i] += vertexAcceleration[i];

                if(vertexHeight[i] + vertexVelocity[i] / resolutionOfParticlesMovement > particleMaximumHeight)
                {
                    vertexHeight[i] = particleMaximumHeight;
                }
                else if( vertexHeight[i] + vertexVelocity[i] / resolutionOfParticlesMovement <= particleMaximumHeight && vertexHeight[i] + vertexVelocity[i] / resolutionOfParticlesMovement >= -particleMaximumHeight )
                {
                    vertexHeight[i] += vertexVelocity[i] / resolutionOfParticlesMovement;
                }
                else
                {
                    vertexHeight[i] = -particleMaximumHeight;
                }

                vertexHeight[i] += shifting;
            }
        }

        void generateBitmap()
        {
            if(bitmap == null || bitmap.Width != wavePoolSize)
            {
                bitmap = new Bitmap(wavePoolSize, wavePoolSize, PixelFormat.Format24bppRgb);
            }

            bitmapData = bitmap.LockBits(new Rectangle(0, 0, wavePoolSize, wavePoolSize),
                                         ImageLockMode.WriteOnly,
                                         PixelFormat.Format24bppRgb);

            IntPtr pointer = bitmapData.Scan0; // przypisanie adresu 1 pixela w bitmapie do wskaźnika
            int bytes = bitmapData.Stride * bitmapData.Height; // Stride zwraca rozmiar lini w bitach
            byte[] rgbData = new byte[bytes];

            for(int i = 0; i < vertexHeight.Length; i++)
            {
                // ustalanie jasności w zależności od wysokości
                byte brightness = (byte)((vertexHeight[i] + particleMaximumHeight) / ((particleMaximumHeight * 2f) / 255f));

                if(isObstacleVertex[i])
                {
                    rgbData[i * 3] = ObstacleColor.B;
                    rgbData[i * 3 + 1] = ObstacleColor.G;
                    rgbData[i * 3 + 2] = ObstacleColor.R;
                }
                else
                {
                    float bright1 = (float)brightness / 255f;
                    float bright2 = 1f - (float)brightness / 255f;

                    rgbData[i * 3] = (byte)((float)color1.B * bright1 + (float)color2.B * bright2);
                    rgbData[i * 3 + 1] = (byte)((float)color1.G * bright1 + (float)color2.G * bright2);
                    rgbData[i * 3 + 2] = (byte)((float)color1.R * bright1 + (float)color2.R * bright2);
                }
            }

            Marshal.Copy(rgbData, 0, pointer, bytes);
            bitmap.UnlockBits(bitmapData);
        }

        public void setParticles(Rectangle rectangle, float value, ParticleAttribute particleAttribute)
        {
            mutex.WaitOne();

            if(rectangle.X < 0)
            {
                rectangle.X = 0;
            }

            if(rectangle.Y < 0)
            {
                rectangle.Y = 0;
            }

            if(rectangle.Width + rectangle.X > wavePoolSize)
            {
                rectangle.Width -= (rectangle.X + rectangle.Width) - wavePoolSize;
            }

            if (rectangle.Height + rectangle.Y > wavePoolSize)
            {
                rectangle.Height -= (rectangle.Y + rectangle.Height) - wavePoolSize;
            }

            bool isHeight = false;
            bool isVelocity = false;
            bool isAcceleration = false;
            bool isSustainability = false;
            bool isFixity = false;

            if( (ParticleAttribute.ALL & particleAttribute) == ParticleAttribute.ALL )
            {
                isHeight = true;
                isVelocity = true;
                isAcceleration = true;
                isSustainability = true;
                isFixity = true;
            }
            else
            {
                if ((ParticleAttribute.HEIGHT & particleAttribute) == ParticleAttribute.HEIGHT)
                {
                    isHeight = true;
                }

                if ((ParticleAttribute.VELOCITY & particleAttribute) == ParticleAttribute.VELOCITY)
                {
                    isVelocity = true;
                }

                if ((ParticleAttribute.ACCELERATION & particleAttribute) == ParticleAttribute.ACCELERATION)
                {
                    isAcceleration = true;
                }

                if ((ParticleAttribute.SUSTAINABILITY & particleAttribute) == ParticleAttribute.SUSTAINABILITY)
                {
                    isSustainability = true;
                }

                if ((ParticleAttribute.FIXITY & particleAttribute) == ParticleAttribute.FIXITY)
                {
                    isFixity = true;
                }
            }

            for(int y = rectangle.Y * wavePoolSize; y < rectangle.Y * wavePoolSize + rectangle.Height * wavePoolSize; y += wavePoolSize)
            {
                for(int x = rectangle.X; x < rectangle.X + rectangle.Width; x++)
                {
                    if(isHeight)
                    {
                        vertexHeight[x + y] = value;
                    }

                    if(isVelocity)
                    {
                        vertexVelocity[x + y] = value;
                    }
                    
                    if(isAcceleration)
                    {
                        vertexAcceleration[x + y] = value;
                    }

                    if (isSustainability)
                    {
                        vertexSustainability[x + y] = value;
                    }
                    if (isFixity)
                    {
                        isObstacleVertex[x + y] = Convert.ToBoolean(value);
                    }
                }
            }

            mutex.ReleaseMutex();
        }

        void setSustainability()
        {
            float sustainabilityDecreaseRate;
            float currentSustainability;

            if (isEdgeAbsobtion)
            {
                if (minimalSustainability > sustainability)
                {
                    minimalSustainability = 1.0f; // even "sustain" can't be less than 1.0f so this is a reliable value.
                }

                if (absorbtionOffset >= wavePoolSize / 2)
                {
                    absorbtionOffset = wavePoolSize / 2 - 1;
                }

                sustainabilityDecreaseRate = (sustainability - minimalSustainability) / (float)absorbtionOffset;

                currentSustainability = minimalSustainability;

                for (int i = 0; i < vertexSustainability.Length - 1; i++)
                {
                    vertexSustainability[i] = sustainability;
                }

                // This loop sets up the sustainability values for the top.
                for (int i = 0; i <= absorbtionOffset; i++)
                {
                    // Process each row/column from the edge to the offset.
                    for (int j = i; j < wavePoolSize - i; j++)
                    {
                        // Process each sustainability element in the current row/column
                        vertexSustainability[j + i * wavePoolSize] = currentSustainability;
                    }
                    currentSustainability += sustainabilityDecreaseRate;
                }

                currentSustainability = sustainability; // Reset the current sustainability.


                // This loop sets up the sustainability values for the bottom.
                for (int i = 0; i <= absorbtionOffset; i++)
                {
                    for (int j = absorbtionOffset - i; j < wavePoolSize - (absorbtionOffset - i); j++)
                    {
                        vertexSustainability[j + i * wavePoolSize + wavePoolSize * (wavePoolSize - absorbtionOffset - 1)] = currentSustainability;
                    }
                    currentSustainability -= sustainabilityDecreaseRate;
                }


                currentSustainability = sustainability;

                // This loop sets up the sustainability values for the left.
                for (int i = 0; i <= absorbtionOffset; i++)
                {
                    for (int j = absorbtionOffset - i; j < wavePoolSize - (absorbtionOffset - i); j++)
                    {
                        vertexSustainability[j * wavePoolSize + (absorbtionOffset - i)] = currentSustainability;
                    }
                    currentSustainability -= sustainabilityDecreaseRate;
                }

                currentSustainability = sustainability;

                // This loop sets up the sustainability values for the right.
                for (int i = 0; i <= absorbtionOffset; i++)
                {
                    for (int j = absorbtionOffset - i; j < wavePoolSize - (absorbtionOffset - i); j++)
                    {
                        vertexSustainability[j * wavePoolSize + i + wavePoolSize - absorbtionOffset - 1] = currentSustainability;
                    }
                    currentSustainability -= sustainabilityDecreaseRate;
                }
            }
            else
            {
                for (int i = 0; i < vertexSustainability.Length; i++)
                    vertexSustainability[i] = sustainability;
            }
        }

        public void Dispose()
        {
            isThreadWorking = false;
            isDisposed = true;
            ThreadPool.QueueUserWorkItem((Object arg1) =>
            {
                mutex.WaitOne();
                bitmap.Dispose();
                mutex.Close();
            });
        }
    }
}